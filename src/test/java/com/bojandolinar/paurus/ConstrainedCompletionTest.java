package com.bojandolinar.paurus;

import com.bojandolinar.paurus.processor.MixedCallable;
import one.util.streamex.StreamEx;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class ConstrainedCompletionTest {
    private static final double cpuToIdleRatio = 0.5;

    @Test
    public void shouldPreserveOrderPerId() {
        List<Record> result = test(
                Record.of("1", 100),
                Record.of("2", 3),
                Record.of("1", 10),
                Record.of("3", 20),
                Record.of("3", 1000),
                Record.of("2", 50),
                Record.of("2", 20),
                Record.of("1", 20)
        );

        checkResult(result);
    }

    private List<Record> test(Record ... records) {
        ConstrainedCompletionService<String, Record> executorService =
                new ConstrainedCompletionService<>(4);
        for (Record record : records) {
            executorService.submit(record.getMatchId(), new MixedCallable<>(record.getDuration(), cpuToIdleRatio, record));
        }

        try {
            executorService.awaitTermination();
            executorService.addToQueue(Record.POISON);
            executorService.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        List<Record> resultList = new ArrayList<>(records.length);
        try {
            Record record = executorService.take();
            while (record != Record.POISON) {
                resultList.add(record);
                record = executorService.take();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return resultList;
    }

    /**
     * <p>Checks if supplied records are still ordered by {@code Record#creationTime} for
     * each {@code Record#matchId}. Note that records are supplied to the queue in
     * the same order they were created so by transitivity completion order should preserve
     * creation order for each id.</p>
     * @param resultList of records in completion order.
     */
    private void checkResult(List<Record> resultList) {
        Map<String, List<Record>> byId = StreamEx.of(resultList)
                .groupingBy(Record::getMatchId);

        for (Map.Entry<String, List<Record>> entry : byId.entrySet()) {
            List<Instant> orderedBySentTimes = StreamEx.of(entry.getValue()).map(Record::getCreationTime).toList();
            System.out.println("id = " + entry.getKey() + " creation times: " + orderedBySentTimes);
            Assertions.assertThat(entry.getValue()).isSortedAccordingTo(Comparator.comparing(Record::getCreationTime));
        }
    }

    private static class Record {
        private final String matchId;
        private final Instant creationTime;
        private final long duration;
        private static Record POISON = new Record("POISON", -1);

        public static Record of(String matchId, long duration) {
            return new Record(matchId, duration);
        }

        private Record(String matchId, long duration) {
            this.matchId = matchId;
            this.creationTime = Instant.now();
            this.duration = duration;
        }

        public String getMatchId() {
            return matchId;
        }

        public Instant getCreationTime() {
            return creationTime;
        }

        public long getDuration() {
            return duration;
        }

        @Override
        public String toString() {
            return "Record{" +
                    "matchId='" + matchId + '\'' +
                    ", creationTime=" + creationTime +
                    ", duration=" + duration +
                    '}';
        }
    }
}
