package com.bojandolinar.paurus;

public enum RecordHeaders {
    MATCH_ID,
    MARKET_ID,
    OUTCOME_ID,
    SPECIFIERS
}
