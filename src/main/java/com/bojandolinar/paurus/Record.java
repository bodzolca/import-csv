package com.bojandolinar.paurus;

import org.apache.commons.csv.CSVRecord;

import java.time.Instant;

import static com.bojandolinar.paurus.RecordHeaders.MARKET_ID;
import static com.bojandolinar.paurus.RecordHeaders.OUTCOME_ID;
import static com.bojandolinar.paurus.RecordHeaders.SPECIFIERS;

public class Record {
    private final String matchId;
    private final String eventType;
    private final int marketId;
    private final String outcomeId;
    private final String specifiers;
    private final Instant creationTime;

    // object used to stop consuming messages
    public static final Record POISON = new Record(null, null, -1, null, null);

    private Record(String matchId, String eventType, int marketId, String outcomeId, String specifiers) {
        this.matchId = matchId;
        this.eventType = eventType;
        this.marketId = marketId;
        this.outcomeId = outcomeId;
        this.specifiers = specifiers;
        this.creationTime = Instant.now();
    }

    public static Record fromCSVRecord(CSVRecord csvRecord) {
        // BOF bytes mess up the first column, so using index instead of enum
        String wholeMatchId = csvRecord.get(0);
        String[] split = wholeMatchId.split(":");
        String type = split.length > 1 ? split[1] : "";
        String matchId = split.length > 2 ? split[2] : "";
        return new Record(matchId, type, Integer.parseInt(csvRecord.get(MARKET_ID)), csvRecord.get(OUTCOME_ID), csvRecord.get(SPECIFIERS));
    }

    public String getMatchId() {
        return matchId;
    }

    public String getEventType() {
        return eventType;
    }

    public int getMarketId() {
        return marketId;
    }

    public String getOutcomeId() {
        return outcomeId;
    }

    public String getSpecifiers() {
        return specifiers;
    }

    public boolean isPoisoned() {
        return this == POISON;
    }

    /**
     * @return Average time in milliseconds that processing of the record of
     * this type takes.
     */
    public int getAverageRuntime() {
        switch (this.getEventType()) {
            case "match":
                return 5;
            case "stage":
                return 10;
            case "simple_tournament":
                return 20;
            case "season":
                return 100;
        }

        return 0;
    }

    @Override
    public String toString() {
        return "Record{" +
                "matchId='" + matchId + '\'' +
                ", eventType='" + eventType + '\'' +
                ", marketId=" + marketId +
                ", outcomeId='" + outcomeId + '\'' +
                ", specifiers='" + specifiers + '\'' +
                ", creationTime=" + creationTime +
                '}';
    }

    public String toStringShort() {
        return "Record{" +
                "matchId='" + matchId + '\'' +
                ", creationTime=" + creationTime +
                '}';
    }
}
