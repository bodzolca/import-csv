package com.bojandolinar.paurus;

import com.bojandolinar.paurus.processor.CSVProcessor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Objects;
import java.util.Properties;

/**
 * <p>This class performs simple ETL. (E)xtracts data from csv file (or stream), (T)ransforms it
 * with unknown processing (in this case we just consume CPU time) and (L)oads it into database.</p>
 * <p>E and T parts are done with {@link CSVProcessor}, L part by {@link DBImporter}, acting as
 * producer and consumer, respectively.</p>
 */
public class Importer {
    private static final double cpuToIdleRatio = 0.5;
    public static final int POOL_SIZE = (int) (Runtime.getRuntime().availableProcessors() / cpuToIdleRatio + 1);
    public static final String SETTINGS_FILE = "settings.properties";
    private ConstrainedCompletionService<String, Record> processorExecutor;

    public static void main(String[] args ) {
        new Importer().readFile();
    }

    private Properties readProperties() {
        Properties properties = new Properties();
        try (InputStream is = getClass().getClassLoader().getResourceAsStream(SETTINGS_FILE)) {
            properties.load(Objects.requireNonNull(is, SETTINGS_FILE + " not found on classpath"));
        } catch (IOException e) {
            System.out.println("Error opening " + SETTINGS_FILE + " file, exiting :" + e.toString());
            System.exit(1);
        }

        return properties;
    }

    private void readFile() {
        Runtime.getRuntime().addShutdownHook(new Importer.ShutdownHook());

        Properties properties = readProperties();

        String property = properties.getProperty("csv-file-path", "<csv-file-path_not-configured>");
        File csvFile = new File(property);
        if (!csvFile.exists()) {
            System.out.println("File " + csvFile + " doesn't exist");
            return;
        }

        try (Reader csvReader = new BufferedReader(new InputStreamReader(new FileInputStream(property)))) {
            CSVParser parser = CSVFormat.DEFAULT.withDelimiter('|').withQuote('\'').withRecordSeparator('\n')
                    .withHeader(RecordHeaders.class)
                    .withFirstRecordAsHeader()
                    .parse(csvReader);

            processorExecutor = new ConstrainedCompletionService<>(POOL_SIZE);
            CSVProcessor producer = new CSVProcessor(processorExecutor, parser.iterator(), cpuToIdleRatio);
            DBImporter consumer = new DBImporter(processorExecutor, properties.getProperty("db-url", "<db-url_not-configured"));

            Thread producerThread = new Thread(producer, "producer-thread");
            producerThread.start();
            consumer.run(); // 'run' intentional, I want to run it in this thread

            shutdown();

            if (!consumer.isPoisonedConsumed()) {
                // abnormal termination of consumer, so interrupt producer
                producerThread.interrupt();
            }
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    private void shutdown() {
        System.out.println("Shutting down ...");
        // todo: Save unfinished tasks
        if (processorExecutor != null) {
            processorExecutor.shutdown();
        }
    }

    private class ShutdownHook extends Thread {
        @Override
        public void run() {
            shutdown();
        }
    }
}
