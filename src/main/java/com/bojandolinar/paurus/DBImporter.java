package com.bojandolinar.paurus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * <p>Consumer of messages on the queue.</p>
 */
public class DBImporter implements Runnable {
    public static final int WORK_UNIT_SIZE = 64;
    private final String connectionURL;
    private boolean poisonedConsumed = false;
    private final ConstrainedCompletionService<String, Record> service;

    public DBImporter(ConstrainedCompletionService<String, Record> service, String connectionURL) {
        this.connectionURL = connectionURL;
        this.service = service;
    }

    public boolean isPoisonedConsumed() {
        return poisonedConsumed;
    }

    @Override
    public void run() {
        long begin = System.nanoTime();

        try (Connection conn = DriverManager.getConnection(connectionURL)) {
            conn.setAutoCommit(false);
            PreparedStatement stmt =
                    conn.prepareStatement("INSERT INTO RECORD (match_id, market_id, outcome_id, data_insert, specifiers)" +
                            "                  VALUES (?, ?, ?, ?, ?)");

            long count = 0;
            while (!Thread.currentThread().isInterrupted()) {
                Record record = service.poll();
                if (record == null) {
                    // if not ready we might as well keep DB busy. This is part of the reason
                    // I didn't choose CompletionService.
                    stmt.executeBatch();
                    conn.commit();
                    System.out.println("opportunistic commit at count = " + count);

                    record = service.take();
                }

                if (record.isPoisoned()) {
                    poisonedConsumed = true;
                    System.out.println("Poison pill encountered, interrupting the task");
                    break;
                }

                stmt.setString(1, record.getMatchId());
                stmt.setInt(2, record.getMarketId());
                stmt.setString(3, record.getOutcomeId());
                stmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                stmt.setObject(5, record.getSpecifiers());

                stmt.addBatch();
                count++;

                if (count % WORK_UNIT_SIZE == 0) {
                    stmt.executeBatch();
                    conn.commit();
                    System.out.println("commit at count = " + count);
                }
            }

            stmt.executeBatch();
            conn.commit();
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        } catch (SQLException ex) {
            System.out.println("SQL error " + ex.getSQLState() + ": " + ex.getMessage());
        }

        System.out.printf("Import took %,d milliseconds\n", (System.nanoTime() - begin) / 1_000_000);
    }
}
