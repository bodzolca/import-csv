package com.bojandolinar.paurus;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * <p>A service similar to {@link java.util.concurrent.ExecutorCompletionService} in that
 * it supplies results of completed tasks in the order they complete, but with additional constraint
 * that task with the same id should be ordered in FIFO fashion. So even if task
 * could complete before the task with the same id that was submitted before, it
 * has to wait for its completion.</p>
 * <p>The important difference is that {@link #take()} and its sibling {@link #poll()} doesn't
 * return {@code Future} but the value itself due to some limitations of
 * {@link java.util.concurrent.CompletionStage} API. This is unfortunate because it doesn't
 * give you enough control for abnormal behaviour (exception thrown while executing the task)
 * and is an obvious area for improvement.</p>
 */
public class ConstrainedCompletionService<K, V> {
    public static final int PURGE_DELAY_SECONDS = 10;
    private final ConcurrentMap<K, CompletableFuture<Void>> sentFuturesForId = new ConcurrentHashMap<>();
    private ScheduledExecutorService purgeFuturesService;

    private final ExecutorService eventProcessingService;
    private final BlockingQueue<V> completionQueue;

    public ConstrainedCompletionService(int nThreads) {
        this.eventProcessingService = Executors.newFixedThreadPool(nThreads);
        this.purgeFuturesService = Executors.newScheduledThreadPool(1);
        this.purgeFuturesService.scheduleWithFixedDelay(this::purgeFutureMap, PURGE_DELAY_SECONDS, PURGE_DELAY_SECONDS, TimeUnit.SECONDS);
        this.completionQueue = new LinkedBlockingQueue<>();
    }

    /**
     * <p>Submits a value-returning task for execution and returns a CompletableFuture
     * representing the pending results of the task. Upon completion,
     * the value of the task may be taken or polled. Values are returned in the
     * order of completion except for values of the same supplied {@code key} that
     * are treated as FIFO.</p>
     * @param key the key for constraining the order of the task values.
     * @param callable callable to submit
     * @return a CompletableFuture representing pending completion of the task
     */
    public CompletableFuture<V> submit(K key, Callable<V> callable) {
        final CompletableFuture<V> process = CompletableFuture.supplyAsync(toSupplier(callable), eventProcessingService);

        sentFuturesForId.compute(key, (existingKey, value) -> {
            if (value != null && !value.isDone()) {
                return process.thenAcceptBothAsync(value, (result, ignore) -> completionQueue.add(result), eventProcessingService);
            } else {
                return process.thenAcceptAsync(completionQueue::add, eventProcessingService);
            }
        });

        return process;
    }

    /**
     * <p>Retrieves and removes the value of next completed task,
     * waiting if none are yet present.</p>
     *
     * @return the value as a result of next completed task
     */
    public V take() throws InterruptedException {
        return completionQueue.take();
    }

    /**
     * <p>Retrieves and removes the value of next completed task,
     * or {@code null} if none are yet present.</p>
     *
     * @return the value as a result of next completed task, or {@code null}
     * if none are present.
     */
    public V poll() {
        return completionQueue.poll();
    }

    /**
     * <p>Retrieves and removes the value of next completed task,
     * waiting if necessary up to the specified wait time if none are yet present.</p>
     *
     * @return the value as a result of next completed task, or
     *   {@code null} if the specified waiting time elapses before one is present
     */
    public V poll(long timeout, TimeUnit unit) throws InterruptedException {
        return completionQueue.poll(timeout, unit);
    }

    public void shutdown() {
        purgeFuturesService.shutdown();
        eventProcessingService.shutdown();
        sentFuturesForId.clear();
    }

    /**
     * <p>This blocking method waits for all submitted tasks to finish.</p>
     */
    public void awaitTermination() throws InterruptedException, ExecutionException {
        purgeFutureMap();
        Collection<CompletableFuture<Void>> futures = sentFuturesForId.values();
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).get();
    }

    /**
     * <p>Adds value to the queue without invoking standard {@link #submit(Object, Callable)}
     * method. Can be used to add poison pill after making sure all submitted
     * tasks are finished (via {@link #awaitTermination()} method).</p>
     * @param value to add directly to queue.
     */
    public void addToQueue(V value) {
        completionQueue.add(value);
    }

    /**
     * Removes all CompletableFutures that are already done.
     */
    private void purgeFutureMap() {
        // warn: this works correctly (is thread-safe) with java 9u65 or later, rewrite if you have to run it with java 8
        // https://bugs.java.com/bugdatabase/view_bug.do?bug_id=8078645
        sentFuturesForId.entrySet().removeIf(entry -> entry.getValue().isDone());
    }

    private static <T>  Supplier<T> toSupplier(Callable<T> callable) {
        return () -> {
            try {
                return callable.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        };
    }
}