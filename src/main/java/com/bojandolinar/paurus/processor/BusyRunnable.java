package com.bojandolinar.paurus.processor;

/**
 * <p>This runnable consumes cpu time of specified duration in milliseconds.</p>
 */
public class BusyRunnable implements Runnable {
    final private long duration; // in milliseconds

    /**
     * @param duration in milliseconds
     */
    public BusyRunnable(long duration) {
        this.duration = duration;
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();
        while (System.nanoTime() - startTime < duration * 1_000_000 && !Thread.currentThread().isInterrupted());
    }
}
