package com.bojandolinar.paurus.processor;

import java.util.concurrent.Callable;

/**
 * <p>This runnable consumes time of specified duration in milliseconds:
 * cpuToIdleRation part spends cpu time, the rest is idle time. </p>
 */
public class MixedCallable<V> implements Callable<V> {
    final private long duration; // in milliseconds
    final private double cpuToIdleRatio;
    final private V value;

    /**
     * @param duration in milliseconds
     * @param cpuToIdleRatio
     * @param value
     */
    public MixedCallable(long duration, double cpuToIdleRatio, V value) {
        this.duration = duration;
        this.cpuToIdleRatio = cpuToIdleRatio;
        this.value = value;
    }

    @Override
    public V call() throws Exception {
        new MixedRunnable(duration, cpuToIdleRatio).run();
        return value;
    }
}
