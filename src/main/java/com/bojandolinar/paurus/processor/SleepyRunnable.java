package com.bojandolinar.paurus.processor;

/**
 * <p>This runnable takes specified time to run without consuming cpu time.</p>
 */
public class SleepyRunnable implements Runnable {
    final private long duration; // in milliseconds

    /**
     * @param duration in milliseconds
     */
    public SleepyRunnable(long duration) {
        this.duration = duration;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
