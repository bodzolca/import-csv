package com.bojandolinar.paurus.processor;

/**
 * <p>This runnable consumes time of specified duration in milliseconds:
 * cpuToIdleRation part spends cpu time, the rest is idle time. </p>
 */
public class MixedRunnable implements Runnable {
    final private long duration; // in milliseconds
    final private double cpuToIdleRatio;

    /**
     * @param duration in milliseconds
     * @param cpuToIdleRatio
     */
    public MixedRunnable(long duration, double cpuToIdleRatio) {
        this.duration = duration;
        this.cpuToIdleRatio = cpuToIdleRatio;
    }

    @Override
    public void run() {
        long sleepDuration = (long) (duration * (1 - cpuToIdleRatio));
        try {
            Thread.sleep(sleepDuration);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        long startTime = System.nanoTime();
        while (System.nanoTime() - startTime < (duration - sleepDuration) * 1_000_000 && !Thread.currentThread().isInterrupted());
    }
}
