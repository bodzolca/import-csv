package com.bojandolinar.paurus.processor;

import com.bojandolinar.paurus.Record;

import java.util.function.Consumer;

public class StdOuputConsumer implements Consumer<Void> {
    private final Record record;

    public StdOuputConsumer(Record record) {
        this.record = record;
    }

    @Override
    public void accept(Void aVoid) {
        System.out.println("Sending " + record.toStringShort());
    }
}
