package com.bojandolinar.paurus.processor;

import com.bojandolinar.paurus.ConstrainedCompletionService;
import com.bojandolinar.paurus.Record;
import org.apache.commons.csv.CSVRecord;

import java.util.Iterator;
import java.util.concurrent.ExecutionException;

/**
 * <p>Producer of messages on the queue.</p>
 */
public class CSVProcessor implements Runnable {

    private final ConstrainedCompletionService<String, Record> service;
    private final Iterator<CSVRecord> iterator;
    private final double cpuToIdleRatio;

    public CSVProcessor(ConstrainedCompletionService<String, Record> service, Iterator<CSVRecord> iterator, double cpuToIdleRatio) {
        this.iterator = iterator;
        this.service = service;
        this.cpuToIdleRatio = cpuToIdleRatio;
    }

    @Override
    public void run() {
        while (iterator.hasNext() && !Thread.currentThread().isInterrupted()) {
            Record record = Record.fromCSVRecord(iterator.next());

            MixedCallable<Record> callable = new MixedCallable<>(record.getAverageRuntime(), cpuToIdleRatio, record);
            service.submit(record.getMatchId(), callable);
        }

        try {
            service.awaitTermination();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        service.addToQueue(Record.POISON);
    }
}
