package com.bojandolinar.paurus;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class ConstrainedCompletionImporter {
    private static final Random RANDOM = new Random(12);
    private static final double cpuToIdleRatio = 0.5;
    public static final int POOL_SIZE = (int) (Runtime.getRuntime().availableProcessors() / cpuToIdleRatio + 1);


    public static void main(String[] args) {
        new ConstrainedCompletionImporter().readFile();
    }

    private void readFile() {
        try (Reader csvReader = new BufferedReader(new InputStreamReader(new FileInputStream("/home/bojan/fo_random_veryshort.txt")))) {
            CSVParser parser = CSVFormat.DEFAULT.withDelimiter('|').withQuote('\'').withRecordSeparator('\n')
                    .withHeader(RecordHeaders.class)
                    .withFirstRecordAsHeader()
                    .parse(csvReader);

            long runtimeTotal = 0;
            long begin = System.nanoTime();

            ConstrainedCompletionService executorService = new ConstrainedCompletionService(POOL_SIZE);
            for (CSVRecord csvRecord : parser) {
                final Record record = Record.fromCSVRecord(csvRecord);

                final int processingDuration = randomizeRuntime(record.getAverageRuntime());
                runtimeTotal += processingDuration;
//                executorService.submit(record.getMatchId(), new MixedRunnable(processingDuration, cpuToIdleRatio), new StdOuputConsumer(record));
            }

            executorService.awaitTermination();
            executorService.shutdown();

            System.out.printf("runtimeTotal = %,d : cpu time = %d\n", runtimeTotal, (int)(runtimeTotal * cpuToIdleRatio));
            System.out.printf("Processing took %,d milliseconds\n", (System.nanoTime() - begin) / 1_000_000);


        } catch (IOException e) {
            System.out.println(e.toString());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static int randomizeRuntime(int averageRuntime) {
        return (int) Math.max(RANDOM.nextGaussian() * averageRuntime / 2 + averageRuntime, 1);
    }
}
