CREATE TABLE RECORD (
   task_id INT AUTO_INCREMENT PRIMARY KEY,
   match_id VARCHAR(255) NOT NULL,
   market_id INT NOT NULL,
   outcome_id VARCHAR(255) NOT NULL,
   data_insert TIMESTAMP NOT NULL,
   specifiers VARCHAR(255)
);