Introduction
---

Simple ETL standalone app that was done as a job interview challenge.
It reads CSV file, does some dummy processing and writes the records
into DB, which in my case was MySQL.

Problem statement
---

The order of imported records has to be retained for each matchid 
(numerical part of first field in a row). Maintaining the
order of source records (csv file) would satisfy the requirement 
but is too restrictive when processed records have to wait unnecessarily
for unprocessed records with different matchid. This is a common scenario
because time of processing could vary considerably depending on type of records.

The logic for this requirement is encapsulated in ConstrainedCompletionService class.

Notes
---

App is not production ready since it only takes care of the nominal process, so exception handling 
is rudimentary as is logging (just System.out). However, I did take some extra care to implement 
termination, but is still not reliable (see TODOs).

Building and running
---

Create a package:

```mvn clean package```

Find package `import-csv-<version>-dist.zip` in `target` folder. 
Extract zip, cd into `import-csv-<version>`. 

Change database url and input file in `conf/settings.properties`.
DDL file for MySQL is ImportTable.sql. After all is set run:
 
```java -jar import-csv-<version>.jar```
